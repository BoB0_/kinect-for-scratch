import edu.ufl.digitalworlds.j4k.J4KSDK;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.ServerSocket;

public class Main extends Application {

    private static KinectListener kinectListener;
    private static ScratchSocket scratchSocket;

    public static void socketError(){
        System.out.println("Port 51298 is in use.");
        System.out.println("Socket error");
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Kinect2Scratch");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();

        try {
            ServerSocket tester = new ServerSocket(51298);
            tester.close();
        } catch (IOException e) {
            socketError();
            return;
        }

        kinectListener = new KinectListener(Byte.valueOf("1"));
        kinectListener.start(J4KSDK.SKELETON);
        System.out.println("Kinect initialized: " + kinectListener.isInitialized());

        scratchSocket = new ScratchSocket(51298);
        scratchSocket.setKinect(kinectListener);
    }

    @Override
    public void stop() throws IOException {
        scratchSocket.socket.close();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
