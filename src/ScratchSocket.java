import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ScratchSocket {
    private BufferedReader input;
    private static OutputStream output;
    public static ServerSocket socket;
    private static Socket client;
    private String line;

    private KinectListener kinectListener;

    private ExecutorService pool;

    public boolean isConnected = false;

    private final String HTTP_HEADER = "HTTP/1.1 200 OK\r\n"
            + "Content-Type: text/html; charset=ISO-8859-1\r\n"
            + "Access-Control-Allow-Origin: *\r\n\r\n";

    private final String HTTP_POLICY = "<cross-domain-policy>\n"
            + "  <allow-access-from domain=\"*\" to-ports=\"*\"/>\n"
            + "</cross-domain-policy>\n\0\r\n";

    public ScratchSocket(int port) {
        pool = Executors.newCachedThreadPool();

        try {
            socket = new ServerSocket(port);
            System.out.println("Starting Scratch socket");
        } catch (IOException e) {
            System.out.println("Error starting Socket");
            return;
        }

        pool.submit(new HttpListener());
    }

    class HttpListener implements Runnable {

        @Override
        public void run() {
            try {

                client = socket.accept();
                output = client.getOutputStream();
                input = new BufferedReader(new InputStreamReader(client.getInputStream()));

                if ((line = input.readLine()) != null) {

                    if (line.contains("crossdomain.xml")) {

                        send(HTTP_POLICY);

                    } else if (line.contains("poll")) {

                        if (!isConnected) {
                            isConnected = true;
                            System.out.println("Scratch 2.0 connected");
                            socket.setSoTimeout(1000);
                        }
                            send(kinectListener.data.toString());
                    }
                } else {
                    send("null\n");
                }

                output.flush();
                output.close();
                client.close();

                pool.submit(new HttpListener());

            } catch (SocketTimeoutException e) {
                isConnected = false;
                System.out.println("Scratch 2.0 disconnected");
                try {
                    socket.setSoTimeout(0);
                } catch (SocketException e1) {
                    e1.printStackTrace();
                }
                pool.submit(new HttpListener());
            } catch (IOException e1) {
            }
        }
    }

    public void send(String data) {

        try {
            output.write((HTTP_HEADER + data).getBytes());
        } catch (IOException e) {
        }
    }

    public void setKinect(KinectListener kinect) {
        kinectListener = kinect;
    }
}