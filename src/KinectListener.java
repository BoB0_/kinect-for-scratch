import edu.ufl.digitalworlds.j4k.J4KSDK;
import edu.ufl.digitalworlds.j4k.Skeleton;

/**
 * Created by Super Rafał on 02.06.2016.
 */
public class KinectListener extends J4KSDK {

    private Skeleton[] skeletons;
    public StringBuilder data = new StringBuilder();

    private final String[] BODY_NAMES = {
            "hip-center",
            "spine",
            "shoulder-center",
            "head",
            "shoulder-left",
            "elbow-left",
            "wrist-left",
            "hand-left",
            "shoulder-right",
            "elbow-right",
            "wrist-right",
            "hand-right",
            "hip-left",
            "knee-left",
            "ankle-left",
            "foot-left",
            "hip-right",
            "knee-right",
            "ankle-right",
            "foot-right"
    };

    public KinectListener(byte kinect_type){
        super(kinect_type);
    }

    @Override
    public void onSkeletonFrameEvent(boolean[] skeleton_tracked, float[] positions, float[] orientations, byte[] joint_status) {
        skeletons = getSkeletons();

        for (int i =0; i<skeletons.length; i++){
            if(skeletons[i].isTracked()){
                buildData(i);
                break;
            }
        }

        System.out.println(data.toString());
    }

    @Override
    public void onColorFrameEvent(byte[] color_frame) {

    }

    @Override
    public void onDepthFrameEvent(short[] depth_frame, byte[] body_index, float[] xyz, float[] uv) {

    }

    public void buildData(int skeletonId){
        Skeleton s = skeletons[skeletonId];

        if(isInitialized() && s.isTracked()){
            data =  new StringBuilder();

            for(int i=0; i < BODY_NAMES.length; i++){
                data.append(BODY_NAMES[i] + "-x " + Math.round(s.get3DJointX(i)*100) + "\n");
                data.append(BODY_NAMES[i] + "-y " + Math.round(s.get3DJointY(i)*100) + "\n");
            }
        }
    }
}
